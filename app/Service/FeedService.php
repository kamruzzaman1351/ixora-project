<?php

namespace App\Service;


use App\Exceptions\AppErrorException;
use App\Feed;
use Illuminate\Http\Request;

class FeedService
{

    public function storeFeed(Request $request)
    {
        $request->validate([
            'name'      => 'required|string|min:4|max:100',
            'feed_body' => 'required|string|min:4|max:500',

        ]);

        $data = $request->all();

        $feed = Feed::create($data);

        if($feed==null){
            throw new AppErrorException("something wrong");

        }
        return $feed;
    }


    public function updateFeed($id,Request $request)
    {
        $request->validate([
            'name'      => 'required|string|min:4|max:100',
            'feed_body' => 'required|string|min:5|max:350'
        ]);

        $feed = Feed::findOrFail($id);

        $feed->name      = $request->name;
        $feed->feed_body = $request->feed_body;

        if(!$feed->isDirty()){
            throw new AppErrorException("You didn't change anything");
        }

        $feed->update();
        return $feed;
    }
}
