<?php

namespace App\Service;


use App\Exceptions\AppErrorException;
use App\Feed;
use App\Comment;
use Illuminate\Http\Request;

class CommentService
{

    public function storeComment($feed_id,Request $request)
    {
        $request->validate([
            'name'     => 'required|string|min:5|max:100',
            'com_body' => 'required|string|min:5|max:300'

        ]);

        $feed = Feed::findOrFail($feed_id);
        $comment = new Comment();
        $comment->name = $request->name;
        $comment->com_body = $request->com_body;
        $comment->feed()->associate($feed);

        $comment->save();

        if($comment==null){
            throw new AppErrorException("something wrong");

        }
        return $comment;
    }


    public function updateComment($id,Request $request)
    {
        $request->validate([
            'name'     => 'required|string|min:5|max:100',
            'com_body' => 'required|string|min:5|max:300'
        ]);

        $comment = Comment::findOrFail($id);

        $comment->name = $request->name;
        $comment->com_body = $request->com_body;

        if(!$comment->isDirty()){
            throw new AppErrorException("You didn't change anything");
        }

        $comment->save();
        return $comment;
    }
}
