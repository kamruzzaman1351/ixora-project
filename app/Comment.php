<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'com_body', 'name', 'feed_id'
    ];



    public function feed()
    {
        return $this->belongsTo(Feed::class);
    }
}
