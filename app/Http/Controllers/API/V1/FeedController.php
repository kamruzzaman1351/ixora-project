<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feed;
use App\Comment;
use App\User;
use App\Exceptions\AppErrorException;
use App\Service\FeedService;
use App\Traits\ApiResponse;
use Auth;

class FeedController extends Controller
{

    use ApiResponse;

    /**
     * @var FeedService
     */
    private $feedService;

    /**
     * FeedController constructor.
     */
    public function __construct(FeedService $feedService)
    {
        $this->feedService = $feedService;
        $this->middleware('auth:api');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index( Request $request )
    {
        $feeds = Feed::with('comment')->latest()->paginate(15);

        return $feeds;


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->feedService->storeFeed($request);
        return $this->showMessage('New Feed Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feed = Feed::with('comment')->find($id);
        if($feed==null){
            throw new AppErrorException("Feed not Found");
        }
        return $feed;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->feedService->updateFeed($id,$request);
        return $this->showMessage('Feed Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feed = Feed::findOrFail($id);
        if($feed==null){
            throw new AppErrorException('This feed does not exit');
        }
        $feed->delete();
        return $this->showMessage('Feed deleted successfully');

    }
}
