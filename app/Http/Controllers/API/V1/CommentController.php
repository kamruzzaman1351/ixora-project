<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Feed;
use App\Comment;
use App\User;
use App\Exceptions\AppErrorException;
use App\Service\CommentService;
use App\Traits\ApiResponse;
use Auth;

class CommentController extends Controller
{
    use ApiResponse;

    /**
     * @var CommentService
     */
    private $commentService;

    /**
     * CommentController constructor.
     */
    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::with('feed')->latest()->paginate(15);
        return $comments;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($feed_id,Request $request)
    {
        $this->commentService->storeComment($feed_id,$request);
        return $this->showMessage('New comment added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->feedService->updateComment($id,$request);
        return $this->showMessage('Comment Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        if($feed==null){
            throw new AppErrorException('This comment does not exit');
        }
        $feed->delete();
        return $this->showMessage('comment deleted successfully');
    }
}
