/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment';

/**
 * vForm
 */
import {
    Form,
    HasError,
    AlertError
} from 'vform'

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)



/**
 *  Vue Routes
 */

import VueRouter from 'vue-router'

Vue.use(VueRouter)

let routes = [{
        path: '/dashboard',
        component: require('./components/Dashboard.vue').default
    },
    {
        path: '/users',
        component: require('./components/User.vue').default
    },
    {
        path: '/feeds',
        component: require('./components/Feed.vue').default
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
});

Vue.component('example-component', require('./components/ExampleComponent.vue').default);


/**
 * Vue Filter
 */

Vue.filter('capitalize', function (text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});

Vue.filter('date', function (date) {
    return moment(date).format("MMM Do YY");
});
Vue.filter('feedDate', function (date) {
    return moment(date).format('MMMM Do YYYY, h:mm:ss a');
});

/**
 * Vue Progress Bar
 */
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '4px'
})

/**
 * Sweet Alert
 */
import Swal from 'sweetalert2'
window.Swal = Swal;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.Toast = Toast;

/**
 * Custom Event
 */

window.Event = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
