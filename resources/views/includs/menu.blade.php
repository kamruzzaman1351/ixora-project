<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>Dashboard</p>
            </router-link>
        </li>
        <li class="nav-item">
            <router-link to="/users" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>All Users</p>
            </router-link>
        </li>
        <li class="nav-item">
            <router-link to="/feeds" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>Feeds</p>
            </router-link>
        </li>
        <li class="nav-item">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
          document.getElementById('logout-form').submit();"
               class="nav-link">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>{{ __('Logout') }}</p>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </li>
    </ul>
</nav>
