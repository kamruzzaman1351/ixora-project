<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    Route::get('/users', 'API\V1\UserController@index');
    Route::apiResource('feeds', 'API\V1\FeedController');
    // Route::apiResource('comments', 'API\V1\CommentController');
    Route::post('comments/{feed_id}', ['uses' => 'API\V1\CommentController@store', 'as' => 'comment.store']);

});

